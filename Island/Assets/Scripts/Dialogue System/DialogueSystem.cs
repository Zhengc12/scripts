﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    [Header("UI Component")]
    public Text textBox;
    public Image charaImage;
    [Header("Texts")]
    public TextAsset textFile;
    public int index;
    public float textSpeed;

    [Header("Profile")]
    public Sprite Faceone, Facetwo;
    bool textFinished;
    bool cancelTyping;

    List<string> textList = new List<string>();
    // Start is called before the first frame update
    void Awake()
    {
        GetTextFormFile(textFile);
    }
    private void OnEnable()
    {
        //textBox.text = textList[index];
        //index++;
        textFinished = true;
        StartCoroutine(SetTextUI());
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && index == textList.Count)
        {
            gameObject.SetActive(false);
            index = 0;
            return;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (textFinished && !cancelTyping)
            {
                StartCoroutine(SetTextUI());
            }
            else if (!textFinished)
            {
                cancelTyping = !cancelTyping;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
            index = 0;
        }
    }

    void GetTextFormFile(TextAsset file)
    {
        textList.Clear();
        index = 0;

        string[] lineData = file.text.Split(new string[] { "\n", "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);

        foreach (var line in lineData)
        {
            textList.Add(line);
        }
    }

    IEnumerator SetTextUI()
    {
        textFinished = false;
        textBox.text = "";
        int letter = 0;
        SwitchFace();
        while (!cancelTyping && letter < textList[index].Length - 1)
        {
            textBox.text += textList[index][letter];
            letter++;
            yield return new WaitForSeconds(textSpeed);
        }
        textBox.text = textList[index];
        cancelTyping = false;
        textFinished = true;
        index++;
    }

    private void SwitchFace()
    {
        switch (textList[index])
        {
            case "A":
                charaImage.sprite = Faceone;
                index++;
                break;
            case "B":
                charaImage.sprite = Facetwo;
                index++;
                break;
        }
    }
}
